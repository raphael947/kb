<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property int $frequency
 * @property string $name
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'frequency' => 'Frequency',
            'name' => 'Name',
        ];
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle() // is a RELATIONSHIP to the model you wish to attach your tags
    {
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('article_tag_assn', ['tag_id' => 'id']);
    }

    /**
     * HERE IS YOUR method
     * @param string $name
     * @return Tag[]
     */
    public static function findAllByName($name)
    {
        return Tag::find()
            ->where(['like', 'name', $name])->limit(50)->all();
    }
}