<?php

use yii\db\Migration;

/**
 * Handles the creation of table `productSingle`.
 */
class m180415_095227_create_productSingle_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('productSingle', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('productSingle');
    }
}
