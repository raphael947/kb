<?php

namespace app\models;
use dosamigos\taggable\Taggable;
use Yii;
//////////////test////////////
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
//////////////////////////////

/**
 * This is the model class for table "Article".
 *
 * @property int $id
 * @property string $title
 * @property string $descriptin
 * @property string $body
 * @property int $author_id
 * @property int $editor_id
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Article extends \yii\db\ActiveRecord
{
    /////////////////////////////////////////////test////////////////////////

    ////////////////////////////////////////////////////////////////////////
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'safe'],
               [['tagNames'], 'safe'],
        [['author_id', 'editor_id', 'category_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'descriptin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'descriptin' => 'Description',
            'body' => 'Body of article',
            'author_id' => 'Author ID',
            'editor_id' => 'Editor ID',
            'category_id' => 'Category',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
      public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag_assn', ['article_id' => 'id']);
    }
    public function getCategory(){
        return $this->hasOne(Category::className(),['id'=>'category_id']);
    }
    public function findAllByName($name)
    {
        return Tag::find()->where('name LIKE :query')
                          ->addParams([':query'=>"%$name%"])
                          ->all();
    }
    ///////////////////////test//////////////////////////////////////////////////////////
    public function behaviors() {
        return [
            BlameableBehavior::className(),
            [
                'class' => Taggable::className(),
            ],
        ];
    }
    ////////////////////////////////////////////////////////////////////////////////////
}

